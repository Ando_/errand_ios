//
//  ListController.swift
//  Errand
//
//  Created by Shaun Anderson on 8/6/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
import CoreData

class ListController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, ListCellDelegate {

    var tableView: UITableView!
    var lists = [List]()
    
    var curEditing: Bool = false
    
    // MARK: - UIViewController Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor =  UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)

        if lists.count > 0 {
            return
        }
        
        var y: CGFloat = 0;
        RetrieveData()
        
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        
        y += statusBarHeight
        
        let headerView = UIView(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: 200))
        
        let titleLabel = UILabel(frame: CGRect(x: 15, y: headerView.frame.height-75, width: headerView.frame.width, height: 50))
        titleLabel.textAlignment = NSTextAlignment.left
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 32)
        titleLabel.text = "ERRAND"
        
        headerView.addSubview(titleLabel)
        headerView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        
        y += headerView.frame.height
        
        let borderView = UIView(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: 2))
        borderView.backgroundColor = UIColor.black
        
        y += borderView.frame.height
        
        // Create and style tableview
        tableView = UITableView(frame: CGRect(origin: CGPoint(x: 0, y: y), size: CGSize(width: self.view.frame.width, height: self.view.frame.height)))
        tableView.backgroundColor = UIColor.black
        tableView.register(ListCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: y-2, right: 0)
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = 50.0
        tableView.dataSource = self
        tableView.delegate = self
        
        
        self.view.addSubview(headerView)
        self.view.addSubview(borderView)
        self.view.addSubview(tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func SetupMenuBar () {
        let menuBar: MenuBar = MenuBar.init(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        view.addSubview(menuBar)
    }
    
    // MARK: - UITableViewDelegate Methods
    
    var shownIndexes : [IndexPath] = []

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

            
//            cell.transform = CGAffineTransform(translationX: 0, y: 50)
//            cell.layer.shadowColor = UIColor.black.cgColor
//            cell.layer.shadowOffset = CGSize(width: 10, height: 10)
//            cell.alpha = 0
        
        
//            print("editing: \(editingCell)")
//            if(editingCell)
//            {
//                cell.alpha = 0.5
//            }
////
//            UIView.beginAnimations("rotation", context: nil)
//            UIView.setAnimationDuration(1)
//            cell.transform = CGAffineTransform(translationX: 0, y: 0)
//            cell.alpha = 1
//            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
//            UIView.commitAnimations()
        
    }
    
    // MARK: - UITableViewDatasource Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lists.count > 0
        {
            self.tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
            noDataLabel.text = "Pull To Add A List"
            noDataLabel.numberOfLines = 2
            noDataLabel.textColor = UIColor.white
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
        }
        return lists.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            as! ListCell
        cell.selectionStyle = .none
        let item = lists[indexPath.row]
        cell.delegate = self
        cell.list = item
        
        return cell
        
    }
    
    func AddData(text: String)
    {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "List", in: managedContext)!
        
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        
        // Calc date time
        let date = NSDate()
        let calendar = Calendar.current
        
        // Set values
        person.setValue(text, forKeyPath: "title")
        person.setValue(NSUUID(), forKey: "id")
        person.setValue(date, forKey: "createdTimeStamp")
        print("CREATED: id \(person.value(forKey: "id")) : time \(person.value(forKey: "createdTimeStamp"))")
        
        lists.insert(person as! List, at: 0)
        tableView.beginUpdates()
        let indexPathForRow = NSIndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPathForRow as IndexPath], with: .fade)
        tableView.endUpdates()
        
        // enter edit mode
        var editCell: ListCell
        let visibleCells = tableView.visibleCells as! [ListCell]
        for cell in visibleCells {
            if (cell.list === person) {
                editCell = cell
                editCell.label.attributedPlaceholder = NSAttributedString(string: "Enter list name",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
                editCell.label.becomeFirstResponder()
                break
            }
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func RetrieveData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // Set up Request with sorting.
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "List")
        let sectionSortDescriptor = NSSortDescriptor(key: "createdTimeStamp", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        // Fetch the results.
        do {
            lists = try managedContext.fetch(fetchRequest) as! [List]
            print("Retrieved data count = \(lists.count)")
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    // MARK: - UIScrollViewDelegate Methods
    
    let placeHolderCell = ListCell(style: .default, reuseIdentifier: "cell")
    var pullDownInProgress = false
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        // Set up placeHolderCell inital values
        placeHolderCell.itemCompleteLayer.isHidden = true
        placeHolderCell.backgroundColor = UIColor.black
        placeHolderCell.label.textColor = UIColor.white
        
        // Check if the use has dragged form the top of the scrollview and the placeHolderCell
        pullDownInProgress = scrollView.contentOffset.y <= 0.0
        if pullDownInProgress {
            tableView.insertSubview(placeHolderCell, at: 0)
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewContentOffsetY = scrollView.contentOffset.y
        
        if pullDownInProgress && scrollView.contentOffset.y <= 0.0 {
            // maintain the location of the placeholder
            placeHolderCell.frame = CGRect(x: 0, y: -tableView.rowHeight,
                                           width: tableView.frame.size.width, height: tableView.rowHeight)
            placeHolderCell.label.text = -scrollViewContentOffsetY > tableView.rowHeight ?
                "Add new list" : "Pull to add list"
            placeHolderCell.alpha = min(1.0, -scrollViewContentOffsetY / tableView.rowHeight)
        } else {
           // pullDownInProgress = scrollViewContentOffsetY <= 0.0 ? true : false
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // check whether the user pulled down far enough
        if pullDownInProgress && -scrollView.contentOffset.y > tableView.rowHeight {
            AddData(text: "")
        }
        pullDownInProgress = false
        placeHolderCell.removeFromSuperview()
    }
    
    // MARK: - ListCellDelgate Methods
    
    /**
     Delete a list element and reload the TableView with animations.
     - Parameter list: the list to be deleted.
     */
    func ListDeleted(list: List) {
        let index = (lists as NSArray).index(of: list)
        if index == NSNotFound { return }
        
        lists.remove(at: index)
        
        // loop over the visible cells to animate delete
        let visibleCells = tableView.visibleCells as! [ListCell]
        let lastView = visibleCells[visibleCells.count - 1] as ListCell
        var delay = 0.0
        var startAnimating = false
        for i in 0..<visibleCells.count {
            let cell = visibleCells[i]
            if startAnimating {
                UIView.animate(withDuration: 0.3, delay: delay, options: .curveEaseInOut,
                               animations: {() in
                                cell.frame = cell.frame.offsetBy(dx: 0.0, dy: -cell.frame.size.height)},
                               completion: {(finished: Bool) in
                                if (cell == lastView) {
                                    self.tableView.reloadData()
                                }
                }
                )
                delay += 0.03
            }
            if cell.list === list {
                startAnimating = true
            }
        }
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return  }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        print("Delete: \(list.id!)")

        // Delete List
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "List")
        fetchRequest.predicate = NSPredicate(format: "id == %@", list.id! as CVarArg)
        
        if let result = try? managedContext.fetch(fetchRequest) {
            let resultData = result as! [List]
            for object in resultData {
                managedContext.delete(object)
            }
        }
        
        // Delete Errands associated with the list
        let errandFetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Errand")
        errandFetchRequest.predicate = NSPredicate(format: "listID == %@", list.id! as CVarArg)
        if let result = try? managedContext.fetch(errandFetchRequest) {
            let resultData = result as! [Errand]
            for object in resultData {
                managedContext.delete(object)
            }
        }
        
        // Save new context
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        // use the UITableView to animate the removal of this row
        tableView.beginUpdates()
        let indexPathForRow = NSIndexPath(row: index, section: 0)
        tableView.deleteRows(at: [indexPathForRow as IndexPath], with: .fade)
        tableView.endUpdates()
    }
    
    func MoveIntoList(list: List) {
        print("MOVE INTO LIST: \(list.title!)")
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        let newViewController = ErrandController()
        newViewController.list = list
        self.present(newViewController, animated: false, completion: nil)
    }
    
    func cellDidBeginEditing(Cell: ListCell, pullCell: Bool) {

        print("IS PULL CELLL: \(pullCell)")
        if(!pullCell)
        {
            let editingOffset = tableView.contentOffset.y - Cell.frame.origin.y as CGFloat
            print("EIDTING CELL AT: \(Cell.frame.origin.y)")
            let visibleCells = tableView.visibleCells as! [ListCell]
            for cell in visibleCells {
                UIView.animate(withDuration: 0.3, animations: {() in
                    cell.transform = CGAffineTransform(translationX: 0, y: editingOffset)
                    if cell !== Cell {
                        cell.alpha = 0.3
                        cell.isUserInteractionEnabled = false
                    }
                })
            }
        }
        else
        {
            let visibleCells = tableView.visibleCells as! [ListCell]
            for cell in visibleCells {
                UIView.animate(withDuration: 0.3, animations: {() in
                    if cell !== Cell {
                        cell.alpha = 0.3
                        cell.isUserInteractionEnabled = false
                    } else {
                        cell.backgroundColor = UIColor.black
                        cell.label.textColor = UIColor.white
                    }
                })
            }
        }
    }
    
    func cellDidEndEditing(Cell: ListCell) {
        let visibleCells = tableView.visibleCells as! [ListCell]
        for cell: ListCell in visibleCells {
            UIView.animate(withDuration: 0.3, animations: {() in
                cell.transform = CGAffineTransform.identity
                if cell !== Cell {
                    cell.alpha = 1.0
                    cell.isUserInteractionEnabled = true
                } else {
                    cell.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
                    cell.label.textColor = UIColor.black
                }
            })
            
            if(cell == Cell)
            {
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return  }
                
                let managedContext = appDelegate.persistentContainer.viewContext
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "List")
                
                // Predicates
                fetchRequest.predicate = NSPredicate(format: "id == %@", Cell.list!.id! as CVarArg)
                if let result = try? managedContext.fetch(fetchRequest) {
                    let resultData = result as! [List]
                    let managedObject = resultData[0]
                    managedObject.setValue(Cell.label.text, forKey: "title")
                    
                    do { try managedContext.save() } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
            }
        }
        
    }
    
}
