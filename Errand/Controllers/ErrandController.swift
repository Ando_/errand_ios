//
//  ViewController.swift
//  Errand
//
//  Created by Shaun Anderson on 21/3/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
import CoreData

class ErrandController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, ErrandCellDelegate {
    
    var list: List?
    var tableView: UITableView!
    var errands = [Errand]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor =  UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        if errands.count > 0 {
            return
        }
        
        var y: CGFloat = 0;
        RetrieveData()
        
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        
        y += statusBarHeight
        
        // Create headerView containing the back button and the title
        let headerView = UIView(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: 200))
        headerView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)

        let backButton = UIButton(frame: CGRect(x: 10, y: headerView.frame.height-35, width: 20, height: 20))
        backButton.backgroundColor = UIColor.black
        backButton.addTarget(self, action: #selector(backButtonPressed), for: UIControlEvents.touchUpInside)
        headerView.addSubview(backButton)
        
        let titleLabel = UILabel(frame: CGRect(x: 45, y: headerView.frame.height-50, width: headerView.frame.width, height: 50))
        titleLabel.textAlignment = NSTextAlignment.left
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        titleLabel.text = list?.title
        headerView.addSubview(titleLabel)
        
        y += headerView.frame.height
        
        let borderView = UIView(frame: CGRect(x: 0, y: y, width: self.view.frame.width, height: 2))
        borderView.backgroundColor = UIColor.black
        
        y += borderView.frame.height
        
        // Create and style tableview
        tableView = UITableView(frame: CGRect(origin: CGPoint(x: 0, y: y), size: CGSize(width: self.view.frame.width, height: self.view.frame.height)))
        tableView.backgroundColor = UIColor.black
        tableView.register(ErrandCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        tableView.rowHeight = 50.0
        tableView.dataSource = self
        tableView.delegate = self
        
        self.view.addSubview(headerView)
        self.view.addSubview(borderView)
        self.view.addSubview(tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func SetupMenuBar () {
        let menuBar: MenuBar = MenuBar.init(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        view.addSubview(menuBar)
    }
    
    // MARK: - TableView datasource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    @objc func backButtonPressed(sender: AnyObject)
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if errands.count > 0
        {
            self.tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
            noDataLabel.text = "No Errands\nPull To Add"
            noDataLabel.numberOfLines = 2
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
        }
        return errands.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            as! ErrandCell
        cell.selectionStyle = .none
        let item = errands[indexPath.row]
        cell.delegate = self
        cell.errand = item
        return cell
        
    }
    
    // MARK: - UITableViewDelegate Methods
    
    
    func tableView(_ tableView: UITableView,
                   willDisplay cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = UIColor.white
        
    }
    
    // Mark: UIScrollViewDelegate Methods
    
    let placeHolderCell = ErrandCell(style: .default, reuseIdentifier: "cell")
    var pullDownInProgress = false
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // this behavior starts when a user pulls down while at the top of the table
        pullDownInProgress = scrollView.contentOffset.y <= 0.0
        
        placeHolderCell.itemCompleteLayer.isHidden = true
        placeHolderCell.backgroundColor = UIColor.black
        placeHolderCell.label.textColor = UIColor.white
        
        if pullDownInProgress {
            // add the placeholder
            tableView.insertSubview(placeHolderCell, at: 0)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewContentOffsetY = scrollView.contentOffset.y
        
        if pullDownInProgress && scrollView.contentOffset.y <= 0.0 {
            // maintain the location of the placeholder
            placeHolderCell.frame = CGRect(x: 0, y: -tableView.rowHeight,
                                           width: tableView.frame.size.width, height: tableView.rowHeight)
            placeHolderCell.label.text = -scrollViewContentOffsetY > tableView.rowHeight ?
                "Release to add item" : "Pull to add item"
            placeHolderCell.alpha = min(1.0, -scrollViewContentOffsetY / tableView.rowHeight)
        } else {
            pullDownInProgress = scrollViewContentOffsetY <= 0.0 ? true : false
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // check whether the user pulled down far enough
        if pullDownInProgress && -scrollView.contentOffset.y > tableView.rowHeight {
            AddData(text: "")
        }
        pullDownInProgress = false
        placeHolderCell.removeFromSuperview()
    }
    
    //  Mark: TableViewCellDelegate Methods
    
    func ErrandDeleted(errand: Errand) {
        let index = (errands as NSArray).index(of: errand)
        if index == NSNotFound { return }
        
        errands.remove(at: index)
        
        // loop over the visible cells to animate delete
        let visibleCells = tableView.visibleCells as! [ErrandCell]
        let lastView = visibleCells[visibleCells.count - 1] as ErrandCell
        var delay = 0.0
        var startAnimating = false
        for i in 0..<visibleCells.count {
            let cell = visibleCells[i]
            if startAnimating {
                UIView.animate(withDuration: 0.3, delay: delay, options: .curveEaseInOut,
                                           animations: {() in
                                           cell.frame = cell.frame.offsetBy(dx: 0.0, dy: -cell.frame.size.height)},
                                           completion: {(finished: Bool) in

                                            if (cell == lastView) {
                                                self.tableView.reloadData()
                                            }
                }
                )
                delay += 0.03
            }
            if cell.errand === errand {
                startAnimating = true
                cell.isHidden = true
            }
        }
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return  }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Errand")
        
        // Predicates
        fetchRequest.predicate = NSPredicate(format: "id == %@", errand.id! as CVarArg)
        
        if let result = try? managedContext.fetch(fetchRequest) {
            let resultData = result as! [Errand]
            for object in resultData {
                managedContext.delete(object)
                print("DELETE")
            }
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        // use the UITableView to animate the removal of this row
        tableView.beginUpdates()
        let indexPathForRow = NSIndexPath(row: index, section: 0)
        tableView.deleteRows(at: [indexPathForRow as IndexPath], with: .fade)
        tableView.endUpdates()
    }
    
    func cellDidBeginEditing(editingCell: ErrandCell, pullCell: Bool) {
        
        if(!pullCell)
        {
            let editingOffset = tableView.contentOffset.y - editingCell.frame.origin.y as CGFloat
            print("EDITING CELL IS AT: \(editingCell.frame.origin.y)")
            let visibleCells = tableView.visibleCells as! [ErrandCell]
            for cell in visibleCells {
                UIView.animate(withDuration: 0.3, animations: {() in
                    cell.transform = CGAffineTransform(translationX: 0, y: editingOffset)
                    if cell !== editingCell {
                        cell.alpha = 0.3
                    }
                })
            }
        }
        else
        {
            let visibleCells = tableView.visibleCells as! [ErrandCell]
            for cell in visibleCells {
                UIView.animate(withDuration: 0.3, animations: {() in
                    if cell !== editingCell {
                        cell.alpha = 0.3
                    }
                })
            }
        }

    }
    
    func cellDidEndEditing(editingCell: ErrandCell) {
        let visibleCells = tableView.visibleCells as! [ErrandCell]
        for cell: ErrandCell in visibleCells {
            UIView.animate(withDuration: 0.3, animations: {() in
                cell.transform = CGAffineTransform.identity
                if cell !== editingCell {
                    cell.alpha = 1.0
                }
            })
            
            if(cell == editingCell)
            {
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return  }
                
                let managedContext = appDelegate.persistentContainer.viewContext
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Errand")
                
                print(editingCell.errand?.text)
                // Predicates
                fetchRequest.predicate = NSPredicate(format: "id == %@", editingCell.errand!.id! as CVarArg)
                if let result = try? managedContext.fetch(fetchRequest) {
                    let resultData = result as! [Errand]
                    let managedObject = resultData[0]
                    managedObject.setValue(editingCell.label.text, forKey: "text")
                    
                    do { try managedContext.save() } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
            }
        }
        
        //Save
    }
    
    func AddData(text: String)
    {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Errand", in: managedContext)!
        
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        
        // Calc date time
        let date = NSDate()
        let calendar = Calendar.current

        // Set values
        person.setValue(text, forKeyPath: "text")
        person.setValue(false, forKey: "completed")
        person.setValue(NSUUID(), forKey: "id")
        person.setValue(list?.id, forKey: "listID")
        person.setValue(date, forKey: "createdTimeStamp")
        print("CREATED: id \(person.value(forKey: "id")) : time \(person.value(forKey: "createdTimeStamp"))")
        
        errands.insert(person as! Errand, at: 0)
        tableView.beginUpdates()
        let indexPathForRow = NSIndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPathForRow as IndexPath], with: .fade)
        tableView.endUpdates()
        
        // enter edit mode
        var editCell: ErrandCell
        let visibleCells = tableView.visibleCells as! [ErrandCell]
        for cell in visibleCells {
            if (cell.errand === person) {
                editCell = cell
                //editCell.label.placeholder = "ENTER ERRAND NAME"
                editCell.label.attributedPlaceholder = NSAttributedString(string: "ENTER ERRAND NAME",
                                                                       attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
                editCell.label.becomeFirstResponder()
                break
            }
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func RetrieveData() {
        
        print("Retreieve data for : \(list?.title!)")
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Errand")
        let predicate = NSPredicate(format: "listID == %@", list?.id! as! NSUUID)
        fetchRequest.predicate = predicate
        let sectionSortDescriptor = NSSortDescriptor(key: "createdTimeStamp", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        //3
        do {
            errands = try managedContext.fetch(fetchRequest) as! [Errand]
            print("Retrieved data count = \(errands.count)")
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func GoBack()
    {
        dismiss(animated: true, completion: nil)
    }
    
}

