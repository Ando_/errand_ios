//
//  ListCell.swift
//  Errand
//
//  Created by Shaun Anderson on 8/6/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
import CoreData

/**
 This protocol contains the functionaility that the cell will communicate with the ViewController.
 */
protocol ListCellDelegate {
    func ListDeleted(list: List)
    func cellDidBeginEditing(Cell: ListCell, pullCell: Bool)
    func cellDidEndEditing(Cell: ListCell)
    func MoveIntoList(list: List)
}

class ListCell: UITableViewCell, UITextFieldDelegate {
    
    // MARK: - Variables
    
    var originalCenter = CGPoint()
    var deleteOnDragRelease = false, completeOnDragRelease = false
    var label: StrikeThroughText
    var itemCompleteLayer = CALayer()
    var delegate: ListCellDelegate?
    var list: List? {
        didSet {
            label.text = list!.title
            label.strikeThrough = false
            itemCompleteLayer.isHidden = !label.strikeThrough
        }
    }
    let gradientLayer = CAGradientLayer()
    var curEditing: Bool = false
    // MARK: - UITableViewCell Methods
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 50))
        // create a label that renders the to-do item text
        label = StrikeThroughText(frame: CGRect.null)
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.backgroundColor = UIColor.clear
        label.rightView = paddingView
        label.contentVerticalAlignment = .center
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        label.delegate = self
        
        // add a layer that renders a green background when an item is complete
        itemCompleteLayer = CALayer(layer: layer)
        itemCompleteLayer.backgroundColor = UIColor(red: 0.95, green: 0.5, blue: 0.45,alpha: 1.0).cgColor
        layer.insertSublayer(itemCompleteLayer, at: 1)
        addSubview(label)
        
        self.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        self.selectionStyle = .none

        // Add gesture recognizers
        // Pan handler
        let panRecognizer = UIPanGestureRecognizer(target: self, action:#selector(handlePan(recognizer:)))
        panRecognizer.delegate = self
        addGestureRecognizer(panRecognizer)
        // Single tap handler
        let singleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleSingleTap))
        singleTapRecognizer.numberOfTapsRequired = 1
        // Double tap handler
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
        doubleTapRecognizer.numberOfTapsRequired = 2
        singleTapRecognizer.require(toFail: doubleTapRecognizer)

        let dummieView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width + 100.0, height: 50))
        dummieView.addGestureRecognizer(doubleTapRecognizer)
        dummieView.addGestureRecognizer(singleTapRecognizer)
        addSubview(dummieView)
        
    }
    
    let kLabelLeftMargin: CGFloat = 15.0
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // ensure the gradient layer occupies the full bounds
        gradientLayer.frame = bounds
        itemCompleteLayer.frame = bounds
        label.frame = CGRect(x: kLabelLeftMargin, y: 0,
                             width: bounds.size.width - kLabelLeftMargin,
                             height: bounds.size.height)
    }
    
    override func prepareForReuse() {
        itemCompleteLayer.isHidden = true
        self.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        
    }
    
    //MARK: - UIPanGestureRecognizer Methods
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        
        if recognizer.state == .began {
            originalCenter = center
        }
        if recognizer.state == .changed {
            let translation = recognizer.translation(in: self)
            if (translation.x < 0) {
                center = CGPoint(x: originalCenter.x + translation.x, y: originalCenter.y)
            }
            switch (frame.origin.x)
            {
            case let x where x < -frame.size.width / 2.0:
                self.backgroundColor = UIColor(red: 0.95, green: 0.5, blue: 0.45,alpha: 1.0)
                break
            default:
                self.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
                break
            }
            deleteOnDragRelease = frame.origin.x < -frame.size.width / 2.0
            completeOnDragRelease = frame.origin.x > frame.size.width / 2.0
        }
        if recognizer.state == .ended {
            let originalFrame = CGRect(x: 0, y: frame.origin.y,
                                       width: bounds.size.width, height: bounds.size.height)
            if deleteOnDragRelease {
                if delegate != nil && list != nil {
                    delegate!.ListDeleted(list: list!)
                }
            } else if completeOnDragRelease {
                UIView.animate(withDuration: 0.2, animations: {self.frame = originalFrame})
            } else {
                UIView.animate(withDuration: 0.2, animations: {self.frame = originalFrame})
            }
        }
    }
    
    @objc func handleSingleTap()
    {
        if(!curEditing){
            print("Single tap")
            delegate?.MoveIntoList(list: self.list!)
        }
    }
    
    @objc func handleDoubleTap()
    {
        if(!curEditing){
            print("Double tap")
            label.becomeFirstResponder()
        }
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: superview!)
            if fabs(translation.x) > fabs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
    
    // MARK: - UITextFieldDelegate methods
    
    func textFieldDidEndEditing(_ textField: UITextField!) {
        curEditing = false
        if delegate != nil {
            delegate!.cellDidEndEditing(Cell: self)
        }
        if list != nil {
            if textField.text == "" {
                delegate!.ListDeleted(list: list!)
            } else {
                list!.title = textField.text
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        curEditing = false
        // close the keyboard on Enter
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        curEditing = true
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        curEditing = true
        if delegate != nil {
            let isNew: Bool = textField.text == "" ? true : false
            delegate!.cellDidBeginEditing(Cell: self, pullCell: isNew)
            print("IT IS \(isNew)")
        }
    }
    
}
