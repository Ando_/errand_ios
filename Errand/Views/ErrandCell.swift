//
//  ErrandCell.swift
//  Errand
//
//  Created by Shaun Anderson on 21/3/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit
import QuartzCore
import CoreData

// A protocol that the TableViewCell uses to inform its delegate of state change
protocol ErrandCellDelegate {
    func ErrandDeleted(errand: Errand)
    func cellDidBeginEditing(editingCell: ErrandCell, pullCell: Bool)
    func cellDidEndEditing(editingCell: ErrandCell)
}

class ErrandCell: UITableViewCell, UITextFieldDelegate {
    
    var originalCenter = CGPoint()
    var deleteOnDragRelease = false, completeOnDragRelease = false
    var label: StrikeThroughText
    var itemCompleteLayer = CALayer()
    var delegate: ErrandCellDelegate?
    
    // The item that this cell renders.
    var errand: Errand? {
        didSet {
            label.text = errand!.text
            label.strikeThrough = errand!.completed
            itemCompleteLayer.isHidden = !label.strikeThrough
        }
    }
    
    let gradientLayer = CAGradientLayer()
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    
    override func prepareForReuse() {
        itemCompleteLayer.isHidden = true
        self.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        self.label.textColor = UIColor.black
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 50))
        // create a label that renders the to-do item text
        label = StrikeThroughText(frame: CGRect.null)
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.backgroundColor = UIColor.clear
        label.rightView = paddingView
        label.contentVerticalAlignment = .center
        


        super.init(style: style, reuseIdentifier: reuseIdentifier)
        label.delegate = self

        // add a layer that renders a green background when an item is complete
        itemCompleteLayer = CALayer(layer: layer)
        itemCompleteLayer.backgroundColor = UIColor(red: 0.0, green: 0.6, blue: 0.0,alpha: 1.0).cgColor
        itemCompleteLayer.isHidden = true
        layer.insertSublayer(itemCompleteLayer, at: 0)
        
        // add a pan recognizer
        let recognizer = UIPanGestureRecognizer(target: self, action:#selector(handlePan(recognizer:)))
        recognizer.delegate = self
        addGestureRecognizer(recognizer)
        
        addSubview(label)

        
        let doubleTapReognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
        doubleTapReognizer.numberOfTapsRequired = 2
        // TODO: Change
        let dummieView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width + 100.0, height: 50))
        dummieView.addGestureRecognizer(doubleTapReognizer)
        addSubview(dummieView)

        /* gradient layer for cell
         gradientLayer.frame = bounds
         let color1 = UIColor(white: 1.0, alpha: 0.2).cgColor as CGColor
         let color2 = UIColor(white: 1.0, alpha: 0.1).cgColor as CGColor
         let color3 = UIColor.clear.cgColor as CGColor
         let color4 = UIColor(white: 0.0, alpha: 0.1).cgColor as CGColor
         gradientLayer.colors = [color1, color2, color3, color4]
         gradientLayer.locations = [0.0, 0.01, 0.95, 1.0]
         layer.insertSublayer(gradientLayer, at: 0)
         */
        // remove the default blue highlight for selected cells
        selectionStyle = .none
    }
    
    let kLabelLeftMargin: CGFloat = 15.0
    override func layoutSubviews() {
        super.layoutSubviews()
        // ensure the gradient layer occupies the full bounds
        gradientLayer.frame = bounds
        itemCompleteLayer.frame = bounds
        label.frame = CGRect(x: kLabelLeftMargin, y: 0,
                             width: bounds.size.width - kLabelLeftMargin,
                             height: bounds.size.height)
    }
    
    //MARK: - horizontal pan gesture methods
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        
        // Begun
        if recognizer.state == .began {
            // when the gesture begins, record the current center location
            originalCenter = center
        }
        // Changed
        if recognizer.state == .changed {
            let translation = recognizer.translation(in: self)
            center = CGPoint(x: originalCenter.x + translation.x, y: originalCenter.y)
            // has the user dragged the item far enough to initiate a delete/complete?
            switch (frame.origin.x)
            {
            case let x where x < -frame.size.width / 2.0:
                self.backgroundColor = UIColor(red: 0.95, green: 0.5, blue: 0.45,alpha: 1.0)
                break
            case let x where x > frame.size.width / 2.0:
                self.backgroundColor = UIColor(red: 0.6, green: 0.9, blue: 0.5,alpha: 1.0)
                break
            default:
                self.backgroundColor = UIColor.white
                break
            }
            deleteOnDragRelease = frame.origin.x < -frame.size.width / 2.0
            completeOnDragRelease = frame.origin.x > frame.size.width / 2.0
        }
        // Ended
        if recognizer.state == .ended {
            let originalFrame = CGRect(x: 0, y: frame.origin.y,
                                       width: bounds.size.width, height: bounds.size.height)
            if deleteOnDragRelease {
                if delegate != nil && errand != nil {
                    // notify the delegate that this item should be deleted
                    delegate!.ErrandDeleted(errand: errand!)
                }
            } else if completeOnDragRelease {
                if errand != nil {
                    errand!.completed = true
                    
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return  }
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Errand")
                    
                    // Predicates
                    fetchRequest.predicate = NSPredicate(format: "id == %@", errand!.id! as CVarArg)
                    if let result = try? managedContext.fetch(fetchRequest) {
                        let resultData = result as! [Errand]
                        let managedObject = resultData[0]
                        managedObject.setValue(errand!.completed, forKey: "completed")
                        
                        do { try managedContext.save() } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                }
                label.strikeThrough = true
                itemCompleteLayer.isHidden = false
                
                UIView.animate(withDuration: 0.2, animations: {self.frame = originalFrame})
            } else {
                UIView.animate(withDuration: 0.2, animations: {self.frame = originalFrame})
            }
        }
    }
    
    @objc func handleDoubleTap()
    {
        print("Double tap")
        label.becomeFirstResponder()
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: superview!)
            if fabs(translation.x) > fabs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
    
    // MARK: - UITextFieldDelegate methods
    
    func textFieldDidEndEditing(_ textField: UITextField!) {
        if delegate != nil {
            delegate!.cellDidEndEditing(editingCell: self)
        }
        if errand != nil {
            if textField.text == "" {
                delegate!.ErrandDeleted(errand: errand!)
            } else {
                errand!.text = textField.text
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // close the keyboard on Enter
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // disable editing of completed to-do items
        if errand != nil {
            return !errand!.completed
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if delegate != nil {
            let isNew: Bool = textField.text == "" ? true : false
            delegate!.cellDidBeginEditing(editingCell: self, pullCell: isNew)
            print("IT IS \(isNew)")
        }
    }
    
}
