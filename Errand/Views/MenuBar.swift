//
//  MenuBar.swift
//  Errand
//
//  Created by Shaun Anderson on 5/5/18.
//  Copyright © 2018 Shaun Anderson. All rights reserved.
//

import UIKit;

class MenuBar: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
